<?php
include 'includes/elements/header.php';

?>

<section class="statistique conteneur">
     <h2>Grands Champions du tournoi Phortnot</h2>
     <div class="stat-content"></div>
     <?php
     $stat = tournoi_info();
     foreach ($stat as $type) {
          if ($type['champion'] == 1) {
               echo '<div class="flex stat-line">';
               echo '<div class="images">';
               echo '<div class="color"><img src="img/profil/perso' . $type['id_image'] . '.jpg" alt="image profil"></div>';
               echo '</div>';
               echo '<div class="details">';
               echo '<h3>' . $type['username'] . '</h3>';
               echo '<p>Victoires: ' . $type['win'] . '</p>';
               echo '<p>Défaites: ' . $type['lose'] . '</p>';
               echo '<p>Champion: ' .  ($type['champion'] == 0 ? 'non' : 'oui');
               echo '</p>';
               echo '</div>';
               echo '</div>';
          }
     } ?>


</section>

<?php
require 'includes/elements/footer.php';
?>