-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 04 juin 2020 à 22:03
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `phortnot`
--

-- --------------------------------------------------------

--
-- Structure de la table `image_profil`
--

DROP TABLE IF EXISTS `image_profil`;
CREATE TABLE IF NOT EXISTS `image_profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lien` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `image_profil`
--

INSERT INTO `image_profil` (`id`, `lien`) VALUES
(1, 'img/profil/perso1.jpg'),
(2, 'img/profil/perso2.jpg'),
(3, 'img/profil/perso3.jpg'),
(4, 'img/profil/perso4.jpg'),
(5, 'img/profil/perso5.jpg'),
(6, 'img/profil/perso6.jpg'),
(7, 'img/profil/perso7.jpg'),
(8, 'img/profil/perso8.jpg'),
(9, 'img/profil/perso9.jpg'),
(10, 'img/profil/perso10.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `profil`
--

DROP TABLE IF EXISTS `profil`;
CREATE TABLE IF NOT EXISTS `profil` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `last_name` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `first_name` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(40) COLLATE utf8mb4_general_ci NOT NULL,
  `birth` date DEFAULT NULL,
  `username` varchar(40) COLLATE utf8mb4_general_ci NOT NULL,
  `pwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_image` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `profil`
--

INSERT INTO `profil` (`id`, `last_name`, `first_name`, `email`, `phone`, `birth`, `username`, `pwd`, `id_image`, `created_at`) VALUES
(1, 'Warlock', 'Oxzane', 'ox@media3.com', '2147483647', '1949-05-13', 'Oxzane', '$2y$10$3PfmUzvZBBreM3zBGit0AuOddFaF.1W/UJOuu1WSQWBfeY6P8kM2y', '9', '2020-05-31 02:46:46'),
(2, 'Willer', 'Midajah', 'kil@ya.com', '1234567891', '1980-01-13', 'Kylia', '$2y$10$vXYOgS7.opQHr4vRl/MI3eeogmZ4RT8PSadb1UMDamVWCtw9tfxCy', '10', '2020-05-31 17:06:28'),
(3, 'Valygar', 'Maddy', 'maddy@val-gar.co', '1234567891', '1997-05-21', 'Maddy', '$2y$10$qRG9WuKjfcWGsNkez4miXOXzGI485VfyAw6Ph7U2tINVmUGK/98Nq', '7', '2020-05-31 17:07:32'),
(4, 'Athkins', 'Torie', 'torie@gmail.com', '2147483647', '2012-05-18', 'Torie', '$2y$10$PUz5ZGdSOGOdrnC6U4IYvOLZvhy/mz/yKrPh0kZ/DslXMZQEU7iwa', '10', '2020-05-31 17:08:25'),
(5, 'Serra', 'Raziel', 'raz@raz.com', '5818971514', '2001-11-23', 'Raziel', '$2y$10$opOdaBGLsRJkSrslVQNpOOC603k.f0ceZoTTOdTK9TjN659So3Bje', '6', '2020-05-31 17:09:08'),
(6, 'Lyhrel', 'Floppy', 'flop@pyy.com', '2147483647', '1996-06-19', 'Floppy', '$2y$10$aMnyLNiMqp/pd/5pnvgvB.j1Cec7oZ/l12p3JdWTJmYjyhv75jJRe', '8', '2020-05-31 17:10:11'),
(7, 'Elge', 'Belkira', 'belki@lg.com', '2147483647', '1989-10-08', 'Belkira', '$2y$10$noJL/I.5KiYVBsV/80vxDeLMG5pOXSuRrmdcUJbRy/yZH/KDP1XcW', '5', '2020-05-31 17:11:57'),
(8, 'Cy', 'Issel', 'iss@elll.saa', '1234567891', '1991-02-03', 'Issel', '$2y$10$ZljzdyQG9639HV3/.JOuVOBaeIwpmbu0/KbYFtZOQDhQCf8IkpJTK', '4', '2020-05-31 21:23:39'),
(69, 'val', 'val', 'val@val.com', '4189854545', '2020-06-25', 'val', '$2y$10$zkrIcvelPJVGdgtGi/MWm.wU3qYYCMMe7sfAusVQ5sjEsT7tYVeKu', '1', '2020-06-04 03:00:57');

-- --------------------------------------------------------

--
-- Structure de la table `tournoi`
--

DROP TABLE IF EXISTS `tournoi`;
CREATE TABLE IF NOT EXISTS `tournoi` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_profil` int(10) UNSIGNED NOT NULL,
  `win` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lose` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `champion` int(10) UNSIGNED NOT NULL,
  `death` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profil_id` (`id_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `tournoi`
--

INSERT INTO `tournoi` (`id`, `id_profil`, `win`, `lose`, `champion`, `death`) VALUES
(1, 1, 15, 20, 0, NULL),
(2, 2, 7, 25, 0, NULL),
(3, 3, 26, 14, 1, '2020-05-08'),
(4, 4, 7, 6, 0, NULL),
(5, 5, 47, 39, 1, '2019-06-11'),
(6, 6, 456, 329, 1, '2017-03-13'),
(7, 7, 137, 135, 0, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `tournoi`
--
ALTER TABLE `tournoi`
  ADD CONSTRAINT `fk_profil_id` FOREIGN KEY (`id_profil`) REFERENCES `profil` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
